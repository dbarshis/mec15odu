# Molecular Ecology SP15

This repository contains the documentation, datasets, and assignments for Dan Barshis' Molecular Ecology course (Biology 453-553) at Old Dominion University, as taught in the spring of 2015.

See the [syllabus](https://bitbucket.org/dbarshis/mec15odu/src/master/syllabus/MECSyllabus_ODU_Barshis_FA14_v8.pdf) for additional information.

The book for the class is:

[Allendorf, Fred W., Gordon Luikart, and Sally N. Aitken. 2013. Conservation and the genetics of populations. 2nd Edition. Wiley-Blackwell](http://www.wiley.com/WileyCDA/WileyTitle/productCd-EHEP002672.html), West Sussex, UK

Note that having the second edition is important. You will be responsible for the material in the second edition.

## Course description

This course will explore the ecology of organisms using molecular techniques and data. Molecular Ecology covers a wide variety of sub-disciplines within Biology, including genetics, physiology, ecology, and evolution. This course will explore basic theory in population genetics, ecology, and evolution and cover DNA, RNA, and Protein techniques and their application to investigations of population ecology and organismal evolution.

A major portion of the lab section for this course will be a specialized group project. The instructor will provide ideas for projects at the beginning of the course, and students are encouraged to change/modify projects with discussion with the rest of the class and instructor. Projects are subject to instructor approval and logistic/expense limitations, but the goal is to generate a substantive, creative inquiry that approaches a real-world question or problem.

## Learning Objectives

1. You will be able to identify the major types of genetic variation, their function in the genome, and their utility for molecular ecological analyses
2. You will be able to explain how drift, mutation, migration, and selection shape patterns of variation across the genome
3. You will be able to summarize the strengths and limitations of molecular ecological methods, as well as the scientific questions that can be addressed
4. Starting from raw data, you will be able to perform standard molecular analyses

## Assignments

- See syllabus

##External links

- check back for updates